### includes
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

keepOutput = True # Debug

### helpers
def MassToFloat(s):
  if "p" in s:
    return float(s.replace("p", "."))
  return float(s)

### parse job options name 
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short().split('_')
evgenLog.info("Physics short names: " + str(phys_short))
#additional_options = phys_short.split("_")[4:]
#evgenLog.info("DirectStau: Additional options: " + str(additional_options))

#C1/N2/N3 degenerate
masses['1000025'] = -1.0 * MassToFloat(phys_short[3]) # N3
masses['1000023'] = MassToFloat(phys_short[3]) # N2
masses['1000024'] = MassToFloat(phys_short[3]) # C1
#N1
masses['1000022'] = MassToFloat(phys_short[4]) # N1
if masses['1000022']<0.5: masses['1000022']=0.5

# interpret the generation type.
gentype = phys_short[1] # should be always N2N3
if gentype not in ["C1C1","C1N2","C1N3"]:
    raise Exception(gentype+" is not supported")

############################
# Updating the parameters
############################

#
# C1,N2,N3 branching ratio
#
decaytype = phys_short[2] #e.g. ZZ,Zh,hh,Zh50
if decaytype not in ["WW","WZ","Wh"]:
    raise Exception(decaytype+" is not supported")

if decaytype == 'WZ':
  evgenLog.info('Force Br(N2->ZN1)=Br(N3->ZN1)=100%')
  decays['1000023'] = """DECAY   1000023     2.05122242E-01   # neutralino2 decays
  #       BR              NDA     ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     0.00000000E+00    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
  #
  """
  decays['1000025'] = """DECAY   1000025     2.10722495E-01   # neutralino3 decays
  #          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     0.00000000E+00    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
  #
  """
  decays['1000024'] = """DECAY   1000024     2.10722495E-01   # chargino1 decays
  #          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_30 -> ~chi_10   W )
  #
  """
elif decaytype == 'Wh':
  evgenLog.info('Force Br(N2->ZN1)=Br(N3->hN1)=100%')
  decays['1000023'] = """DECAY   1000023     2.05122242E-01   # neutralino2 decays
  #       BR              NDA     ID1       ID2
     0.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     1.00000000E+00    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
  #
  """
  decays['1000025'] = """DECAY   1000025     2.10722495E-01   # neutralino3 decays
  #          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.00000000E+00    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
  #
  """
  decays['1000024'] = """DECAY   1000024     2.10722495E-01   # chargino1 decays
  #          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_30 -> ~chi_10   W )
  #
  """
elif decaytype == 'WW': 
  evgenLog.info('Only set Br(C1)')
  decays['1000024'] = """DECAY   1000024     2.10722495E-01   # chargino1 decays
  #          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        24   # BR(~chi_30 -> ~chi_10   W )
  #
  """

# Mixing matrix for the Higgsino NLSP / Bino LSP / decoupled Wino
# Derived from  Spheno
#  1    2.00000000E+02  # M1input
#  2    5.00000000E+03  # M2input
#  3    5.00000000E+03  # M3input
# 23    8.00000000E+02  # Muinput
# 25    1.00000000E+01  # TanBeta
# 26    5.00000000E+03  # MAinput
#
# Off-diagonal chargino mixing matrix V
param_blocks['VMIX']={}
param_blocks['VMIX']['1 1']='0.00E+00'
param_blocks['VMIX']['1 2']='1.00E+00'
param_blocks['VMIX']['2 1']='1.00E+00'
param_blocks['VMIX']['2 2']='0.00E+00'
# Off-diagonal chargino mixing matrix U
param_blocks['UMIX']={}
param_blocks['UMIX']['1 1']='0.00E+00'
param_blocks['UMIX']['1 2']='1.00E+00'
param_blocks['UMIX']['2 1']='1.00E+00'
param_blocks['UMIX']['2 2']='0.00E+00'
# Neutralino mixing matrix chi_i0 = N_ij i(B,W,H_d,H_u)
param_blocks['NMIX']={}
param_blocks['NMIX']['1 1']=' 1.00E+00' # N_11 bino (N1)
param_blocks['NMIX']['1 2']=' 0.00E+00' # N_12
param_blocks['NMIX']['1 3']=' 0.00E+00' # N_13
param_blocks['NMIX']['1 4']=' 0.00E+00' # N_14
param_blocks['NMIX']['2 1']=' 0.00E+00' # N_21
param_blocks['NMIX']['2 2']=' 0.00E+00' # N_22 
param_blocks['NMIX']['2 3']=' 7.07E-01'  # N_23 higgsino (N2)
param_blocks['NMIX']['2 4']='-7.07E-01' # N_24 higgsino (N2)
param_blocks['NMIX']['3 1']=' 0.00E+00' # N_31
param_blocks['NMIX']['3 2']=' 0.00E+00'  # N_32 
param_blocks['NMIX']['3 3']='-7.07E-01'  # N_33 higgsino (N3)
param_blocks['NMIX']['3 4']='-7.07E-01'  # N_34 higgsino (N3)
param_blocks['NMIX']['4 1']=' 0.00E+00' # N_41
param_blocks['NMIX']['4 2']='-1.00E+00'  # N_42 wino (N4)
param_blocks['NMIX']['4 3']=' 0.00E+00'  # N_43
param_blocks['NMIX']['4 4']=' 0.00E+00' # N_44

#
# Debug the SM higgs mass/branching ratio in the default param_card (which uses the values of 110.8GeV higgs)
# https://gitlab.cern.ch/atlas/athena/-/blob/ee38d2687aa7b57481a12bf0b6abf5461dc1680f/Generators/MadGraphControl/python/MadGraphParamHelpers.py#L120
masses['25'] = 125.10
decays['25'] = """DECAY  25   6.382339e-03
#          BR         NDA      ID1       ID2
    5.767E-01 2   5  -5 # H->bb
    6.319E-02 2  15 -15 # H->tautau
    2.192E-04 2  13 -13 # H->mumu
    2.462E-04 2   3  -3 # H->ss
    2.911E-02 2   4  -4 # H->cc
    8.569E-02 2  21  21 # H->gg
    2.277E-03 2  22  22 # H->gammagamma
    1.539E-03 2  22  23 # H->Zgamma
    2.146E-01 2  24 -24 # H->WW
    2.641E-02 2  23  23 # H->ZZ
""" 

madspindecays=False
if (decaytype == 'WZ' or decaytype == 'Wh' or decaytype == 'WW' ) and ('MadSpin' in jofile) :
  madspindecays = True;

print( "gentype", gentype )
print( "decaytype", decaytype )
print( "decays", decays )
print( "masses", masses )
print( "madspindecays", madspindecays )

# max number of jets will be two, unless otherwise specified.
njets = 2

#--------------------------------------------------------------
# MadGraph options
#
process = '''
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define lv = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~
define f = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ u u~ d d~ c c~ s s~ b b~
'''

msdecaystring=""

if gentype == "C1C1" :
    mergeproc="{x1+,1000024}{x1-,-1000024}"
    process += '''
        generate p p > x1+ x1- $ susystrong @1
        add process p p > x1+ x1- j $ susystrong @2
        add process p p > x1+ x1- j j $ susystrong @3
    '''
    msdecaystring="decay x1+ > w+ n1, w+ > f f \n decay x1- > w- n1, w- > f f \n "
elif gentype == "C1N2" :
    mergeproc="{x1-,-1000024}{n2,1000023}"
    process += '''
        generate p p > x1- n2 $ susystrong @1
        add process p p > x1- n2 j $ susystrong @2
        add process p p > x1- n2 j j $ susystrong @3
    '''
    if decaytype == 'WZ':
        msdecaystring="decay x1- > w- n1, w- > f f \n decay n2 > z n1, z > f f \n"
    elif decaytype == 'Wh':
        msdecaystring="decay x1- > w- n1, w- > f f \n decay n2 > h01 n1\n"
elif gentype == "C1N3" :
    mergeproc="{x1-,-1000024}{n3,1000025}"
    process += '''
        generate p p > x1+ n3 $ susystrong @1
        add process p p > x1+ n3 j $ susystrong @2
        add process p p > x1+ n3 j j $ susystrong @3
    '''
    if decaytype == 'WZ':
        msdecaystring="decay x1- > w- n1, w- > f f \n decay n3 > z n1, z > f f \n"
    elif decaytype == 'Wh':
        msdecaystring="decay x1- > w- n1, w- > f f \n decay n3 > h01 n1\n"

#if madspindecays == True:
#  if decaytype == 'ZZ':
#    msdecaystring="decay n2 > z n1, z > f f \n decay n3 > z n1, z > f f \n "
#  elif decaytype == 'Zh':
#    msdecaystring="decay n2 > z n1, z > f f \n decay n3 > h01 n1 \n "
#  elif decaytype == 'hZ':
#    msdecaystring="decay n3 > z n1, z > f f \n decay n2 > h01 n1 \n "
#  elif decaytype == 'hh':
#    msdecaystring="decay n2 > h01 n1 \n decay n3 > h01 n1 \n "
#  elif decaytype == 'Zh50':
#    msdecaystring="decay n2 > z n1, z > f f \n decay n2 > h01 n1 \n decay n3 > z n1, z > f f \n decay n3 > h01 n1 \n "


# print the process, just to confirm we got everything right
print( "Final process card:" )
print( process )


#--------------------------------------------------------------
# Madspin configuration
#
#if madspindecays==True:
#  if msdecaystring=="":
#    raise RuntimeError("Asking for MadSpin decays, but no decay string provided!")
#  madspin_card='madspin_card.dat'
#
#  mscard = open(madspin_card,'w')
#
#  mscard.write("""#************************************************************
##*                        MadSpin                           *
##*                                                          *
##*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
##*                                                          *
##*    Part of the MadGraph5_aMC@NLO Framework:              *
##*    The MadGraph5_aMC@NLO Development Team - Find us at   *
##*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
##*                                                          *
##************************************************************
##Some options (uncomment to apply)
##
## set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
#set BW_cut 15                # default for onshell
#set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
##
#set seed %i
#set spinmode none
## specify the decay for the final state particles
#
#%s
#
#
## running the actual code
#launch"""%(runArgs.randomSeed,msdecaystring))
#  mscard.close()
#  mergeproc+="LEPTONS,NEUTRINOS"

# information about this generation
evgenLog.info('Registered generation of ~chi1p ~chi30 / ~chi1p ~chi20 / ~chi1p ~chi1p production, decaying into ~chi10 via WZ/Wh/WW; grid point decoded into mass point ' + str(masses['1000025']) + ' ' + str(masses['1000022']))
evgenConfig.contact  = [ "yuchen.cai@cern.ch" ]
evgenConfig.keywords += ['gaugino', 'neutralino']
evgenConfig.description = 'higgsino production, decaying into bino-like ~chi10 via ZZ/Zh/hh in simplified model. m_H = %s GeV, m_B = %s GeV'%(masses['1000025'],masses['1000022'])

#--------------------------------------------------------------
# No filter at the moment
evt_multiplier=2
evgenLog.info('inclusive processes will be generated')

#--------------------------------------------------------------
# Standard post-include
#
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

#--------------------------------------------------------------
# Pythia options
#
evgenLog.info('Using Merging:Process = guess')
genSeq.Pythia8.Commands += ["Merging:Process = guess"] 
genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO'] 

#--------------------------------------------------------------
# Pythia options
#
#pythia = genSeq.Pythia8
#pythia.Commands += ["23:mMin = 0.2"]
#pythia.Commands += ["24:mMin = 0.2"]

#--------------------------------------------------------------
# Merging options
#
#if njets>0:
#    #genSeq.Pythia8.Commands += [ "Merging:Process = pp>{n2,1000023}{n3,1000025}",
#    #                             "1000025:spinType = 1",
#    #                             "1000023:spinType = 1" ]
#    if gentype == "C1C1" :
#        genSeq.Pythia8.Commands += [ "Merging:Process = pp>{x1+,1000024}{x1-,-1000024}",
#                                     "1000024:spinType = 1"]
#    elif gentype == "C1N2" :
#        genSeq.Pythia8.Commands += [ "Merging:Process = pp>{x1-,-1000024}{n2,1000023}",
#                                     "1000024:spinType = 1",
#                                     "1000023:spinType = 1" ]
#    elif gentype == "C1N3" :
#        genSeq.Pythia8.Commands += [ "Merging:Process = pp>{x1-,-1000024}{n3,1000025}",
#                                     "1000024:spinType = 1",
#                                     "1000025:spinType = 1" ]
