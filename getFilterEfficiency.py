import os
import tarfile
import re

def extract_and_cat_logs(base_dir, process_pattern):
    """
    Extract 'log.generate' files from .tgz tarballs in subfolders, 
    and print lines containing 'filter Efficiencies'.
    """
    for subdir, _, files in os.walk(base_dir):
        #if process_pattern in subdir:
        if re.search(process_pattern, subdir) :
            for file in files:
                if file.endswith(".tgz"):
                    tarball_path = os.path.join(subdir, file)
                    
                    # Extract tarball
                    with tarfile.open(tarball_path, 'r:gz') as tar:
                        log_file = None
                        for member in tar.getmembers():
                            if member.name.endswith("log.generate"):
                                log_file = member
                                break
                        
                        if log_file:
                            # Extract log.generate
                            log_content = tar.extractfile(log_file).read().decode('utf-8')
                            
                            # Search for "filter Efficiencies"
                            print(f"From {tarball_path} - {log_file.name}:")
                            for line in log_content.splitlines():
                                if "INFO Filter Efficiency" in line:
                                    print(line)
                            print("-" * 50)  # Separator for clarity
                        else:
                            print(f"No 'log.generate' found in {tarball_path}")

# Example usage
base_directory = "./"

#extract_and_cat_logs(base_directory, "C1C1_WW_.*_1L7not2L_" )
#extract_and_cat_logs(base_directory, "C1mN2_WZ_.*_1L7not2L_")
#extract_and_cat_logs(base_directory, "C1pN2_WZ_.*_1L7not2L_")
#extract_and_cat_logs(base_directory, "C1mN2_Wh_.*_1L7not2L_")
#extract_and_cat_logs(base_directory, "C1pN2_Wh_.*_1L7not2L_")

extract_and_cat_logs(base_directory, "C1C1_WW_.*_2L" )
extract_and_cat_logs(base_directory, "C1mN2_WZ_.*_2L")
extract_and_cat_logs(base_directory, "C1pN2_WZ_.*_2L")
extract_and_cat_logs(base_directory, "C1mN2_Wh_.*_2L")
extract_and_cat_logs(base_directory, "C1pN2_Wh_.*_2L")

