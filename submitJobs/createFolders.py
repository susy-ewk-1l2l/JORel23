import os,shutil


def create_folders(CO_path, CO_name, JO_name, folder_name):
    """
    Creates a folder, copies a specified file, and writes a new file with a specific content.

    Args:
        CO_path (str): Path to the directory containing the file to copy.
        CO_name (str): Name of the file to copy.
        JO_name (str): Name of the file to create inside the new folder.
        folder_name (str): Name of the folder to create.
    """
    try:
        # Create the folder
        os.makedirs(folder_name, exist_ok=True)
        print(f"Created folder: {folder_name}")

        # Copy the CO_name file to the folder
        source_file = os.path.join(CO_path, CO_name)
        destination_file = os.path.join(folder_name, CO_name)
        shutil.copy(source_file, destination_file)
        print(f"Copied {CO_name} to {folder_name}")

        # Create a new file named JO_name in the folder with content 'include(CO_name)'
        jo_file_path = os.path.join(folder_name, JO_name)
        with open(jo_file_path, 'w') as jo_file:
            jo_file.write(f"include('{CO_name}')\n")
            jo_file.write(f"genSeq.Pythia8.useRndmGenSvc = False\n")
        print(f"Created file: {JO_name} in {folder_name}")

        with open("submitEVNT.sh", 'a') as submit_file:
            submit_file.write(f"pathena --trf=\"Gen_tf.py --ecmEnergy=13000 --maxEvents=10000 --randomSeed=123456 --jobConfig=./{folder_name} --outputEVNTFile=%OUT.EVNT.root\" --outDS=user.<username>.{JO_name}\n")

    except Exception as e:
        print(f"Error: {e}")

def create_folders_link(CO_path, CO_name, JO_name, folder_name):
    """
    Creates a folder, copies a specified file, and writes a new file with a specific content.

    Args:
        CO_path (str): Path to the directory containing the file to copy.
        CO_name (str): Name of the file to copy.
        JO_name (str): Name of the file to create inside the new folder.
        folder_name (str): Name of the folder to create.
    """
    try:
        # Create the folder
        os.makedirs(folder_name, exist_ok=True)
        print(f"Created folder: {folder_name}")

        folder_name_str = str(folder_name)
        if folder_name_str.endswith("00001"):
            # Copy the CO_name file to the folder
            source_file = os.path.join(CO_path, CO_name)
            destination_file = os.path.join(folder_name, CO_name)
            shutil.copy(source_file, destination_file)
            print(f"Copied {CO_name} to {folder_name}")
        else:
            # Copy the CO_name file to the folder
            corrected_folder_name = folder_name_str[0:1] + "00001"
            os.symlink( os.path.join("../"+corrected_folder_name, CO_name), os.path.join(folder_name, CO_name) )
            print(f"Linked {CO_name} to {corrected_folder_name}")

        # Create a new file named JO_name in the folder with content 'include(CO_name)'
        jo_file_path = os.path.join(folder_name, JO_name)
        with open(jo_file_path, 'w') as jo_file:
            jo_file.write(f"include('{CO_name}')\n")
            jo_file.write(f"genSeq.Pythia8.useRndmGenSvc = False\n")
        print(f"Created file: {JO_name} in {folder_name}")

        #with open("submitEVNT.sh", 'a') as submit_file:
        #    submit_file.write(f"pathena --trf=\"Gen_tf.py --ecmEnergy=13000 --maxEvents=10000 --randomSeed=123456 --jobConfig=./{folder_name} --outputEVNTFile=%OUT.EVNT.root\" --outDS=user.<username>.{JO_name}\n")
        #with open("spreadsheets.csv", 'a') as submit_file:
        #    submit_file.write(f"{folder_name},{JO_name}\n")

    except Exception as e:
        print(f"Error: {e}")


if __name__ == "__main__":
    # Starting folder number and number of folders to create
    Bino_mass_list = [num for num in range(0, 501, 50)]
    Higgsino_mass_list = [num for num in range(200, 801, 50)] + [num for num in range(900, 1201, 100)]
    masses = []
    for each_H in Higgsino_mass_list :
        for each_B in Bino_mass_list:
            if each_B <= each_H - 150 :
                masses.append( str(each_H)+"_"+str(each_B) )
    count = 100001
    for co_path, co_name in zip(["../WW/100/"],["SUSY_SimplifiedModel_C1C1_WW_MadSpin_1L2L.py"]):
        for mass in masses :
            create_folders_link(co_path, co_name, "mc.MGPy8EG_C1C1_WW_"+mass+"_1L7not2L_MadSpin.py", str(count) )
            count += 1
        for mass in masses :
            create_folders_link(co_path, co_name, "mc.MGPy8EG_C1C1_WW_"+mass+"_2L7_MadSpin.py", str(count) )
            count += 1

    count = 200001
    for co_path, co_name in zip(["../WZ/200/"],["SUSY_SimplifiedModel_C1N2_WZ_MadSpin_1L2L.py"]):
        for mass in masses :
            create_folders_link(co_path, co_name, "mc.MGPy8EG_C1pN2_WZ_"+mass+"_1L7not2L_MadSpin.py", str(count) )
            count += 1
        for mass in masses :
            create_folders_link(co_path, co_name, "mc.MGPy8EG_C1mN2_WZ_"+mass+"_1L7not2L_MadSpin.py", str(count) )
            count += 1
        for mass in masses :
            create_folders_link(co_path, co_name, "mc.MGPy8EG_C1pN2_WZ_"+mass+"_2L7_MadSpin.py", str(count) )
            count += 1
        for mass in masses :
            create_folders_link(co_path, co_name, "mc.MGPy8EG_C1mN2_WZ_"+mass+"_2L7_MadSpin.py", str(count) )
            count += 1

    count = 300001
    for co_path, co_name in zip(["../Wh/300"],["SUSY_SimplifiedModel_C1N2_Wh_MadSpin_1L2L.py"]):
        for mass in masses :
            create_folders_link(co_path, co_name, "mc.MGPy8EG_C1pN2_Wh_"+mass+"_1L7not2L_hbb_MadSpin.py", str(count) )
            count += 1
        for mass in masses :
            create_folders_link(co_path, co_name, "mc.MGPy8EG_C1mN2_Wh_"+mass+"_1L7not2L_hbb_MadSpin.py", str(count) )
            count += 1
        for mass in masses :
            create_folders_link(co_path, co_name, "mc.MGPy8EG_C1pN2_Wh_"+mass+"_2L7_hWW_MadSpin.py", str(count) )
            count += 1
        for mass in masses :
            create_folders_link(co_path, co_name, "mc.MGPy8EG_C1mN2_Wh_"+mass+"_2L7_hWW_MadSpin.py", str(count) )
            count += 1
