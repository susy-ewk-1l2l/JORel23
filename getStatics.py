import os
import itertools
import math

"""
https://indico.cern.ch/event/1416093/contributions/5951608/attachments/2855911/4995122/2024.05.14-1L2L.pdf
"""


def folder_rule(f,x=[]):
    return f(x)

def jo_rule(f,x=[]):
    return f(x)

def jo_content(f,x=[]):
    return f(x)
# Define three useless functions

GetXsection13TeV_C1pN2 = {
        100: 13895.1,
        150: 3273.842779,
        200: 1170.26,
        250: 517.259,
        300: 260.319,
        350: 143.126,
        400: 83.9069,
        450: 51.5901,
        500: 32.9135,
        550: 21.644,
        600: 14.5767,
        650: 10.0188,
        700: 6.99008,
        750: 4.95438,
        800: 3.54111,
        850: 2.55793,
        900: 1.87338,
        950: 1.36565,
        1000: 1.01451,
        1100: 0.55945,
        1200: 0.31567,
        }

GetXsection13TeV_C1mN2 = {
        100: 8766.08,
        150: 1907.04,
        200: 637.14,
        250: 265.255,
        300: 126.627,
        350: 66.3319,
        400: 37.12,
        450: 21.8561,
        500: 13.4441,
        550: 8.52674,
        600: 5.56289,
        650: 3.71232,
        700: 2.5208,
        750: 1.73984,
        800: 1.2175,
        850: 0.862477,
        900: 0.623379,
        950: 0.450722,
        1000: 0.329002,
        1100: 0.180922,
        1200: 0.100687,
        }

GetXsection13TeV_C1C1 = {
        100: 11611.9,
        150: 2612.31,
        200: 902.569,
        250: 387.534,
        300: 190.159,
        350: 102.199,
        400: 58.6311,
        450: 35.3143,
        500: 22.1265,
        550: 14.3134,
        600: 9.49913,
        650: 6.43244,
        700: 4.4387,
        750: 3.10861,
        800: 2.21197,
        850: 1.58356,
        900: 1.15301,
        950: 0.842779,
        1000: 0.621866,
        1100: 0.342626,
        1200: 0.196044,
        }

#GetXsection13p6TeV = {}

def CalcEvents(xsec, eff , bf , lumi ):
    return int(math.ceil(0.001 * xsec * eff * bf * lumi / 10) * 10000)


def main():
    print("Main")
    
    def func_folder(x):
        if len(str(x[0])) == 2:
            return "10"+str(x[0])+str(x[1])
        elif len(str(x[0])) == 3:
            return str(x[0])+str(x[1])
    
    os.system("echo DSID,mc20a,mc20d,mc20e,mc23a,mc23d,mc23e,Comments > spreadsheet.csv" )
    #os.system("echo DSID,physics_short,crossSection_pb > metadata.txt" )

    total_20a = 0
    total_20d = 0
    total_20e = 0
    total_23a = 0
    total_23d = 0
    total_23e = 0
    func_jo = lambda x : "mc.MGPy8EG_C1C1_WW_"+str(x[0])+"_"+str(x[1])+"_1L7not2L_MadSpin.py"#_MadSpin
    i=100001
    tmp_20a = 0
    tmp_20d = 0
    tmp_20e = 0
    tmp_23a = 0
    tmp_23d = 0
    tmp_23e = 0
    Bino_mass_list = [num for num in range(0, 501, 50)]
    Higgsino_mass_list = [num for num in range(200, 801, 50)] + [num for num in range(900, 1201, 100)]
    for each_H in Higgsino_mass_list :
        for each_B in Bino_mass_list:
            if each_B <= each_H - 150 :
                os.system("echo "+str(i)+","+
                        str( CalcEvents( GetXsection13TeV_C1C1[each_H], 0.4, 1, 180) )+","+
                        str( CalcEvents( GetXsection13TeV_C1C1[each_H], 0.4, 1, 220) )+","+
                        str( CalcEvents( GetXsection13TeV_C1C1[each_H], 0.4, 1, 300) )+","+
                        str( CalcEvents( GetXsection13TeV_C1C1[each_H], 0.4, 1, 160) )+","+
                        str( CalcEvents( GetXsection13TeV_C1C1[each_H], 0.4, 1, 140) )+","+
                        str( CalcEvents( GetXsection13TeV_C1C1[each_H], 0.4, 1, 550) )+","+func_jo([each_H,each_B])+" >> "+"spreadsheet.csv" )
                tmp_20a += CalcEvents(   GetXsection13TeV_C1C1[each_H], 0.4, 1, 180)
                tmp_20d += CalcEvents(   GetXsection13TeV_C1C1[each_H], 0.4, 1, 220)
                tmp_20e += CalcEvents(   GetXsection13TeV_C1C1[each_H], 0.4, 1, 300)
                tmp_23a += CalcEvents(   GetXsection13TeV_C1C1[each_H], 0.4, 1, 160)
                tmp_23d += CalcEvents(   GetXsection13TeV_C1C1[each_H], 0.4, 1, 140)
                tmp_23e += CalcEvents(   GetXsection13TeV_C1C1[each_H], 0.4, 1, 550)
                i += 1
    total_20a += tmp_20a
    total_20d += tmp_20d
    total_20e += tmp_20e
    total_23a += tmp_23a
    total_23d += tmp_23d
    total_23e += tmp_23e
    print("C1C1_WW_1L7not2L", tmp_20a, tmp_20d, tmp_20e, tmp_23a, tmp_23d, tmp_23e, tmp_20a+tmp_20d+tmp_20e+tmp_23a+tmp_23d+tmp_23e )
    i=150001
    tmp_20a = 0
    tmp_20d = 0
    tmp_20e = 0
    tmp_23a = 0
    tmp_23d = 0
    tmp_23e = 0
    func_jo = lambda x : "mc.MGPy8EG_C1C1_WW_"+str(x[0])+"_"+str(x[1])+"_2L7_MadSpin.py"#_MadSpin
    Bino_mass_list = [num for num in range(0, 501, 50)]
    Higgsino_mass_list = [num for num in range(200, 801, 50)] + [num for num in range(900, 1201, 100)]
    for each_H in Higgsino_mass_list :
        for each_B in Bino_mass_list:
            if each_B <= each_H - 150 :
                os.system("echo "+str(i)+","+
                        str( CalcEvents( GetXsection13TeV_C1C1[each_H], 0.12, 1, 180) )+","+
                        str( CalcEvents( GetXsection13TeV_C1C1[each_H], 0.12, 1, 220) )+","+
                        str( CalcEvents( GetXsection13TeV_C1C1[each_H], 0.12, 1, 300) )+","+
                        str( CalcEvents( GetXsection13TeV_C1C1[each_H], 0.12, 1, 160) )+","+
                        str( CalcEvents( GetXsection13TeV_C1C1[each_H], 0.12, 1, 140) )+","+
                        str( CalcEvents( GetXsection13TeV_C1C1[each_H], 0.12, 1, 550) )+","+func_jo([each_H,each_B])+" >> "+"spreadsheet.csv" )
                tmp_20a += CalcEvents( GetXsection13TeV_C1C1[each_H], 0.12, 1, 180)
                tmp_20d += CalcEvents( GetXsection13TeV_C1C1[each_H], 0.12, 1, 220)
                tmp_20e += CalcEvents( GetXsection13TeV_C1C1[each_H], 0.12, 1, 300)
                tmp_23a += CalcEvents( GetXsection13TeV_C1C1[each_H], 0.12, 1, 160)
                tmp_23d += CalcEvents( GetXsection13TeV_C1C1[each_H], 0.12, 1, 140)
                tmp_23e += CalcEvents( GetXsection13TeV_C1C1[each_H], 0.12, 1, 550)
                i += 1
    total_20a += tmp_20a
    total_20d += tmp_20d
    total_20e += tmp_20e
    total_23a += tmp_23a
    total_23d += tmp_23d
    total_23e += tmp_23e
    print("C1C1_WW_2L7", tmp_20a, tmp_20d, tmp_20e, tmp_23a, tmp_23d, tmp_23e, tmp_20a+tmp_20d+tmp_20e+tmp_23a+tmp_23d+tmp_23e )
    
    func_jo = lambda x : "mc.MGPy8EG_C1pN2_WZ_"+str(x[0])+"_"+str(x[1])+"_1L7not2L_MadSpin.py"
    i=200001
    tmp_20a = 0
    tmp_20d = 0
    tmp_20e = 0
    tmp_23a = 0
    tmp_23d = 0
    tmp_23e = 0
    Bino_mass_list = [num for num in range(0, 501, 50)]
    Higgsino_mass_list = [num for num in range(200, 801, 50)] + [num for num in range(900, 1201, 100)]
    for each_H in Higgsino_mass_list :
        for each_B in Bino_mass_list:
            if each_B <= each_H - 150 :
                os.system("echo "+str(i)+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.32, 1, 180) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.32, 1, 220) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.32, 1, 300) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.32, 1, 160) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.32, 1, 140) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.32, 1, 550) )+","+func_jo([each_H,each_B])+" >> "+"spreadsheet.csv" )
                tmp_20a += CalcEvents(   GetXsection13TeV_C1pN2[each_H], 0.32, 1, 180)
                tmp_20d += CalcEvents(   GetXsection13TeV_C1pN2[each_H], 0.32, 1, 220)
                tmp_20e += CalcEvents(   GetXsection13TeV_C1pN2[each_H], 0.32, 1, 300)
                tmp_23a += CalcEvents(   GetXsection13TeV_C1pN2[each_H], 0.32, 1, 160)
                tmp_23d += CalcEvents(   GetXsection13TeV_C1pN2[each_H], 0.32, 1, 140)
                tmp_23e += CalcEvents(   GetXsection13TeV_C1pN2[each_H], 0.32, 1, 550)
                i += 1
    total_20a += tmp_20a
    total_20d += tmp_20d
    total_20e += tmp_20e
    total_23a += tmp_23a
    total_23d += tmp_23d
    total_23e += tmp_23e
    print("C1pN2_WZ_1L7not2L", tmp_20a, tmp_20d, tmp_20e, tmp_23a, tmp_23d, tmp_23e, tmp_20a+tmp_20d+tmp_20e+tmp_23a+tmp_23d+tmp_23e )
    
    func_jo = lambda x : "mc.MGPy8EG_C1mN2_WZ_"+str(x[0])+"_"+str(x[1])+"_1L7not2L_MadSpin.py"
    i=300001
    tmp_20a = 0
    tmp_20d = 0
    tmp_20e = 0
    tmp_23a = 0
    tmp_23d = 0
    tmp_23e = 0
    Bino_mass_list = [num for num in range(0, 501, 50)]
    Higgsino_mass_list = [num for num in range(200, 801, 50)] + [num for num in range(900, 1201, 100)]
    for each_H in Higgsino_mass_list :
        for each_B in Bino_mass_list:
            if each_B <= each_H - 150 :
                os.system("echo "+str(i)+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.32, 1, 180) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.32, 1, 220) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.32, 1, 300) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.32, 1, 160) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.32, 1, 140) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.32, 1, 550) )+","+func_jo([each_H,each_B])+" >> "+"spreadsheet.csv" )
                tmp_20a += CalcEvents(   GetXsection13TeV_C1mN2[each_H], 0.32, 1, 180)
                tmp_20d += CalcEvents(   GetXsection13TeV_C1mN2[each_H], 0.32, 1, 220)
                tmp_20e += CalcEvents(   GetXsection13TeV_C1mN2[each_H], 0.32, 1, 300)
                tmp_23a += CalcEvents(   GetXsection13TeV_C1mN2[each_H], 0.32, 1, 160)
                tmp_23d += CalcEvents(   GetXsection13TeV_C1mN2[each_H], 0.32, 1, 140)
                tmp_23e += CalcEvents(   GetXsection13TeV_C1mN2[each_H], 0.32, 1, 550)
                i += 1
    total_20a += tmp_20a
    total_20d += tmp_20d
    total_20e += tmp_20e
    total_23a += tmp_23a
    total_23d += tmp_23d
    total_23e += tmp_23e
    print("C1mN2_WZ_1L7not2L", tmp_20a, tmp_20d, tmp_20e, tmp_23a, tmp_23d, tmp_23e, tmp_20a+tmp_20d+tmp_20e+tmp_23a+tmp_23d+tmp_23e )
    
    func_jo = lambda x : "mc.MGPy8EG_C1pN2_WZ_"+str(x[0])+"_"+str(x[1])+"_2L7_MadSpin.py"
    i=250001
    tmp_20a = 0
    tmp_20d = 0
    tmp_20e = 0
    tmp_23a = 0
    tmp_23d = 0
    tmp_23e = 0
    Bino_mass_list = [num for num in range(0, 501, 50)]
    Higgsino_mass_list = [num for num in range(200, 801, 50)] + [num for num in range(900, 1201, 100)]
    for each_H in Higgsino_mass_list :
        for each_B in Bino_mass_list:
            if each_B <= each_H - 150 :
                os.system("echo "+str(i)+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.14, 1, 180) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.14, 1, 220) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.14, 1, 300) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.14, 1, 160) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.14, 1, 140) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.14, 1, 550) )+","+func_jo([each_H,each_B])+" >> "+"spreadsheet.csv" )
                tmp_20a += CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.14, 1, 180)
                tmp_20d += CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.14, 1, 220)
                tmp_20e += CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.14, 1, 300)
                tmp_23a += CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.14, 1, 160)
                tmp_23d += CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.14, 1, 140)
                tmp_23e += CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.14, 1, 550)
                i += 1
    total_20a += tmp_20a
    total_20d += tmp_20d
    total_20e += tmp_20e
    total_23a += tmp_23a
    total_23d += tmp_23d
    total_23e += tmp_23e
    print("C1pN2_WZ_2L7", tmp_20a, tmp_20d, tmp_20e, tmp_23a, tmp_23d, tmp_23e, tmp_20a+tmp_20d+tmp_20e+tmp_23a+tmp_23d+tmp_23e )

    func_jo = lambda x : "mc.MGPy8EG_C1mN2_WZ_"+str(x[0])+"_"+str(x[1])+"_2L7_MadSpin.py"
    i=350001
    tmp_20a = 0
    tmp_20d = 0
    tmp_20e = 0
    tmp_23a = 0
    tmp_23d = 0
    tmp_23e = 0
    Bino_mass_list = [num for num in range(0, 501, 50)]
    Higgsino_mass_list = [num for num in range(200, 801, 50)] + [num for num in range(900, 1201, 100)]
    for each_H in Higgsino_mass_list :
        for each_B in Bino_mass_list:
            if each_B <= each_H - 150 :
                os.system("echo "+str(i)+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.14, 1, 180) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.14, 1, 220) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.14, 1, 300) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.14, 1, 160) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.14, 1, 140) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.14, 1, 550) )+","+func_jo([each_H,each_B])+" >> "+"spreadsheet.csv" )
                tmp_20a += CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.14, 1, 180)
                tmp_20d += CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.14, 1, 220)
                tmp_20e += CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.14, 1, 300)
                tmp_23a += CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.14, 1, 160)
                tmp_23d += CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.14, 1, 140)
                tmp_23e += CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.14, 1, 550)
                i += 1
    total_20a += tmp_20a
    total_20d += tmp_20d
    total_20e += tmp_20e
    total_23a += tmp_23a
    total_23d += tmp_23d
    total_23e += tmp_23e
    print("C1mN2_WZ_2L7", tmp_20a, tmp_20d, tmp_20e, tmp_23a, tmp_23d, tmp_23e, tmp_20a+tmp_20d+tmp_20e+tmp_23a+tmp_23d+tmp_23e )

    func_jo = lambda x : "mc.MGPy8EG_C1pN2_Wh_"+str(x[0])+"_"+str(x[1])+"_1L7not2L_hbb_MadSpin.py"
    i=400001
    tmp_20a = 0
    tmp_20d = 0
    tmp_20e = 0
    tmp_23a = 0
    tmp_23d = 0
    tmp_23e = 0
    Bino_mass_list = [num for num in range(0, 501, 50)]
    Higgsino_mass_list = [num for num in range(200, 801, 50)] + [num for num in range(900, 1201, 100)]
    for each_H in Higgsino_mass_list :
        for each_B in Bino_mass_list:
            if each_B <= each_H - 150 :
                os.system("echo "+str(i)+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.40, 0.5767, 180) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.40, 0.5767, 220) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.40, 0.5767, 300) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.40, 0.5767, 160) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.40, 0.5767, 140) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.40, 0.5767, 550) )+","+func_jo([each_H,each_B])+" >> "+"spreadsheet.csv" )
                tmp_20a += CalcEvents(   GetXsection13TeV_C1pN2[each_H], 0.40, 0.5767, 180)
                tmp_20d += CalcEvents(   GetXsection13TeV_C1pN2[each_H], 0.40, 0.5767, 220)
                tmp_20e += CalcEvents(   GetXsection13TeV_C1pN2[each_H], 0.40, 0.5767, 300)
                tmp_23a += CalcEvents(   GetXsection13TeV_C1pN2[each_H], 0.40, 0.5767, 160)
                tmp_23d += CalcEvents(   GetXsection13TeV_C1pN2[each_H], 0.40, 0.5767, 140)
                tmp_23e += CalcEvents(   GetXsection13TeV_C1pN2[each_H], 0.40, 0.5767, 550)
                i += 1
    total_20a += tmp_20a
    total_20d += tmp_20d
    total_20e += tmp_20e
    total_23a += tmp_23a
    total_23d += tmp_23d
    total_23e += tmp_23e
    print("C1pN2_Wh_1L7not2L", tmp_20a, tmp_20d, tmp_20e, tmp_23a, tmp_23d, tmp_23e, tmp_20a+tmp_20d+tmp_20e+tmp_23a+tmp_23d+tmp_23e )
    
    func_jo = lambda x : "mc.MGPy8EG_C1mN2_Wh_"+str(x[0])+"_"+str(x[1])+"_1L7not2L_hbb_MadSpin.py"
    i=500001
    tmp_20a = 0
    tmp_20d = 0
    tmp_20e = 0
    tmp_23a = 0
    tmp_23d = 0
    tmp_23e = 0
    Bino_mass_list = [num for num in range(0, 501, 50)]
    Higgsino_mass_list = [num for num in range(200, 801, 50)] + [num for num in range(900, 1201, 100)]
    for each_H in Higgsino_mass_list :
        for each_B in Bino_mass_list:
            if each_B <= each_H - 150 :
                os.system("echo "+str(i)+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.40, 0.5767, 180) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.40, 0.5767, 220) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.40, 0.5767, 300) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.40, 0.5767, 160) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.40, 0.5767, 140) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.40, 0.5767, 550) )+","+func_jo([each_H,each_B])+" >> "+"spreadsheet.csv" )
                tmp_20a += CalcEvents(   GetXsection13TeV_C1mN2[each_H], 0.40, 0.5767, 180)
                tmp_20d += CalcEvents(   GetXsection13TeV_C1mN2[each_H], 0.40, 0.5767, 220)
                tmp_20e += CalcEvents(   GetXsection13TeV_C1mN2[each_H], 0.40, 0.5767, 300)
                tmp_23a += CalcEvents(   GetXsection13TeV_C1mN2[each_H], 0.40, 0.5767, 160)
                tmp_23d += CalcEvents(   GetXsection13TeV_C1mN2[each_H], 0.40, 0.5767, 140)
                tmp_23e += CalcEvents(   GetXsection13TeV_C1mN2[each_H], 0.40, 0.5767, 550)
                i += 1
    total_20a += tmp_20a
    total_20d += tmp_20d
    total_20e += tmp_20e
    total_23a += tmp_23a
    total_23d += tmp_23d
    total_23e += tmp_23e
    print("C1mN2_Wh_1L7not2L", tmp_20a, tmp_20d, tmp_20e, tmp_23a, tmp_23d, tmp_23e, tmp_20a+tmp_20d+tmp_20e+tmp_23a+tmp_23d+tmp_23e )
    
    func_jo = lambda x : "mc.MGPy8EG_C1pN2_Wh_"+str(x[0])+"_"+str(x[1])+"_2L7_hWW_MadSpin.py"
    i=450001
    tmp_20a = 0
    tmp_20d = 0
    tmp_20e = 0
    tmp_23a = 0
    tmp_23d = 0
    tmp_23e = 0
    Bino_mass_list = [num for num in range(0, 501, 50)]
    Higgsino_mass_list = [num for num in range(200, 801, 50)] + [num for num in range(900, 1201, 100)]
    for each_H in Higgsino_mass_list :
        for each_B in Bino_mass_list:
            if each_B <= each_H - 150 :
                os.system("echo "+str(i)+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.22, 0.2146, 180) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.22, 0.2146, 220) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.22, 0.2146, 300) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.22, 0.2146, 160) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.22, 0.2146, 140) )+","+
                        str( CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.22, 0.2146, 550) )+","+func_jo([each_H,each_B])+" >> "+"spreadsheet.csv" )
                tmp_20a += CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.22, 0.2146, 180)
                tmp_20d += CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.22, 0.2146, 220)
                tmp_20e += CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.22, 0.2146, 300)
                tmp_23a += CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.22, 0.2146, 160)
                tmp_23d += CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.22, 0.2146, 140)
                tmp_23e += CalcEvents( GetXsection13TeV_C1pN2[each_H], 0.22, 0.2146, 550)
                i += 1
    total_20a += tmp_20a
    total_20d += tmp_20d
    total_20e += tmp_20e
    total_23a += tmp_23a
    total_23d += tmp_23d
    total_23e += tmp_23e
    print("C1pN2_Wh_2L7", tmp_20a, tmp_20d, tmp_20e, tmp_23a, tmp_23d, tmp_23e, tmp_20a+tmp_20d+tmp_20e+tmp_23a+tmp_23d+tmp_23e )
    
    func_jo = lambda x : "mc.MGPy8EG_C1mN2_Wh_"+str(x[0])+"_"+str(x[1])+"_2L7_hWW_MadSpin.py"
    i=550001
    tmp_20a = 0
    tmp_20d = 0
    tmp_20e = 0
    tmp_23a = 0
    tmp_23d = 0
    tmp_23e = 0
    Bino_mass_list = [num for num in range(0, 501, 50)]
    Higgsino_mass_list = [num for num in range(200, 801, 50)] + [num for num in range(900, 1201, 100)]
    for each_H in Higgsino_mass_list :
        for each_B in Bino_mass_list:
            if each_B <= each_H - 150 :
                os.system("echo "+str(i)+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.22, 0.2146, 180) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.22, 0.2146, 220) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.22, 0.2146, 300) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.22, 0.2146, 160) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.22, 0.2146, 140) )+","+
                        str( CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.22, 0.2146, 550) )+","+func_jo([each_H,each_B])+" >> "+"spreadsheet.csv" )
                tmp_20a += CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.22, 0.2146, 180)
                tmp_20d += CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.22, 0.2146, 220)
                tmp_20e += CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.22, 0.2146, 300)
                tmp_23a += CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.22, 0.2146, 160)
                tmp_23d += CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.22, 0.2146, 140)
                tmp_23e += CalcEvents( GetXsection13TeV_C1mN2[each_H], 0.22, 0.2146, 550)
                i += 1
    total_20a += tmp_20a
    total_20d += tmp_20d
    total_20e += tmp_20e
    total_23a += tmp_23a
    total_23d += tmp_23d
    total_23e += tmp_23e
    print("C1mN2_Wh_2L7", tmp_20a, tmp_20d, tmp_20e, tmp_23a, tmp_23d, tmp_23e, tmp_20a+tmp_20d+tmp_20e+tmp_23a+tmp_23d+tmp_23e )
    
    print( total_20a, total_20d, total_20e, total_23a, total_23d, total_23e, total_20a+total_20d+total_20e+total_23a+total_23d+total_23e )

    return 0



if __name__ == "__main__":
    main()
